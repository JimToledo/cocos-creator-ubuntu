# Cocos Creator Ubuntu Project
_______

This project provides a framework for porting a Cocos Creator 
Windows/Mac build to Ubuntu.


## Requirements
* Vagrant
* Virtualbox


## Setup
To set up the Ubuntu environment:
* Run `vagrant up` to set up the Ubuntu VM environment
* Run `vagrant ssh` to access the Ubuntu VM

The project is mounted to `/opt/cocos-creator-ubuntu`inside the VM.


## Cocos Creator Porting

### Porting from a Windows/Mac version
* Install the target Cocos Creator version on Windows or Mac
* Uncompress an existing Cocos Creator Ubuntu build in the `redist` directory: 
  this uncompressed build serves as the foundation for building the new 
  Ubuntu Electron build
* Replace the contents of the `resources` directory in the Ubuntu build with
  the contents of the `reousrces` directory from the Windows or Mac build
* Replace the native node modules in the `resources` directory with the Ubuntu
  equivalent. These are:
  * sharp@0.17.0 (target directory: `resources/utils/sharp`)


### Technical details
Cocos Creator...

* is built on Electron 1.7.5
* relies on native node modules as listed above


## Running Cocos Creator Ubuntu CLI
The following OS packages must be installed:

* xvfb
* libgtk2.0-0
* libxss1
* libgconf-2-4
* libnss3

Cocos Creator Ubuntu CLI also has runtime requirements:
 
* Cocos Creator requires a screen display - this can be accomplished by
  using an Xvfb (X Virtual Frame Buffer) instance
* Cocos Creator must run as an editor at least so that the language
  settings are properly set up

Once these criteria is met, Cocos Creator Ubuntu CLI operates correctly.

The following [builder shell script](provisioning/cocos_creator_build.sh) incorporates
these build requirements.