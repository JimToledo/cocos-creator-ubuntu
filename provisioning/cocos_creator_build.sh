#!/usr/bin/env bash

SCRIPT_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# TODO: Set the Cocos Creator executable path!
EDITOR=$SCRIPT_PATH/../redist/cocos-creator-2.0.9/cocos-creator

# Make sure that a screen display is running - Cocos Creator needs it
echo "Starting up a screen display - required by Cocos Creator"
Xvfb -ac -screen scrn 640x960x24 :9.0 & XVFB_PID=$!

export DISPLAY=:9.0

# Run Cocos Creator in an interactive mode for 30 seconds so that the language setting is properly set up
echo "Running the editor in interactive mode to ensure that the language setting is properly populated"
$EDITOR & EDITOR_PID=$!
sleep 30;
kill $EDITOR_PID

# Run Cocos Creator as a CLI
echo "Running the editor to build the game"
$EDITOR $@

# Kill the background processes start
kill $XVFB_PID

# Return the exit status code
exit $?